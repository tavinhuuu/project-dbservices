*** Settings ***
Resource         ../Resource/Resource.robot
Test Setup       Open and maximize the browser
Test Teardown    Close Browser

*** Test case ***
Scenario 01: Search existing product
    [Tags]  Exist_Product
    Given I access the website
    When I search a product name  Blouse
    Then the product should be listed in the search result  Blouse    $27.00

Scenario 02: Search non-existent product
    [Tags]  Not_Exist_Product
    Given I access the website
    When I search a product name  Non-existent Product
    Then the page should display the message  No results were found for your search "Non-existent Product"

Scenario 03: Search product by category
    [Tags]  Category
    Given I access the website
    When I search a products name by sub categories   Women   Summer Dresses
    Then the page should contains products of the category

Scenario 04: Add product in cart
    [Tags]  Add_product
    Given I access the website
    When I add product to cart  t-shirt
    Then the product should to be on checkout page   Faded Short Sleeve T-shirts   $16.51

Scenario 05: Create a user on website
    [Tags]  Create_user
    Given I access the website
    When I create account
    Then the page should contains a message welcome
