*** Settings ***
Resource         ../Resource/Resource.robot
Force Tags     API

*** Test case ***
Scenario 01: The call to API Rate with today's date should return a valid contract
    [Tags]  current_day
    Given I format day for today
    When I call the API Rate
    Then the API Rate should return a valid contract  ${dt_current}  @{CURRENCY_RECENT}

Scenario 02: The call to API Rate with yesterday's date should return a valid contract
    [Tags]  yesterday
    Given I format day for yesterday
    When I call the API Rate   ${dt_yesterday}
    Then the API Rate should return a valid contract  ${dt_yesterday}  @{CURRENCY_RECENT}

Scenario 03: The call to API Rate with specific valid date should return a valid contract
    [Tags]  specific_day_in_2010
    Given I set variable for specific day  2010-01-12
    When I call the API Rate   ${specific_day}
    Then the API Rate should return a valid contract  ${specific_day}  @{CURRENCY_2010}

Scenario 04: The call to API Rate with incorrect specific date should return a error result
    [Tags]  specific_day_incorrect_old
    Given I set variable for specific day  1900-01-12
    When I call the API Rate   ${specific_day}
    Then the API Rate should return a error result   There is no data for dates older then 1999-01-04.

Scenario 05: The call to API Rate with correct older date should return a valid contract
    [Tags]  specific_day_correct_old
    Given I set variable for specific day  1999-01-04
    When I call the API Rate   ${specific_day}
    Then the API Rate should return a valid contract  ${specific_day}  @{CURRENCY_OLDER}
