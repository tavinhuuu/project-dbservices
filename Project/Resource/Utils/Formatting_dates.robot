*** Keywords ***
Format current date
   [Arguments]  ${format}
   ${dt_current}=    Get Current Date      result_format=${format}
   Set Test Variable    ${dt_current}

Format yesterday date
  [Arguments]  ${format}
  ${dt_current}=    Get Current Date      result_format=${format}
  ${dt_yesterday}=   Add Time To Date   ${dt_current}     -1 days   result_format=${format}
  Set Test Variable    ${dt_yesterday}

Format tomorrow date
  [Arguments]  ${format}
  ${dt_current}=    Get Current Date      result_format=${format}
  ${dt_tomorrow}=   Add Time To Date   ${dt_current}     +2 days   result_format=${format}
  Set Test Variable    ${dt_tomorrow}
