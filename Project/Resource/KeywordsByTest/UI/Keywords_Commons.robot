*** keywords ***
Open and maximize the browser
  Open Browser   about:blank  ${BROWSER}
  Maximize Browser Window

I access the website
  Go To             ${URL}
  Title Should Be   My Store

I call the API Rate
    [Arguments]  ${DAY}=/latest
    Create Session    API   ${API_Rate}
    ${resp}=  Get Request    API    ${DAY}
    Set Test Variable    ${resp}
