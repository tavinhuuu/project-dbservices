*** keywords ***
The product should be listed in the search result
    [Arguments]  ${PRODUCT}    ${PRICE}
    Check if the product has been listed on the site   ${PRODUCT}   ${PRICE}

the page should display the message
    [Arguments]  ${WARNING_MSG}
    Check error message  ${WARNING_MSG}

the page should contains products of the category
    Check products of the category

the page should contains a message welcome
    Page Should Contain    Welcome to your account. Here you can manage all of your personal information and orders.
    Capture Page Screenshot

Check error message
    [Arguments]  ${WARNING_MSG}
    Wait Until Element Is Visible    //*[@id="center_column"]
    Element Text Should Be           //*[@id="center_column"]//*[@class="alert alert-warning"]      ${WARNING_MSG}
    Capture Page Screenshot

Check displayed sub categories
    Wait Until Element Is Visible    //*[@id="block_top_menu"]//*[@title="Tops"]
    Page Should Contain Link         //*[@id="block_top_menu"]//*[@title="Tops"]
    Page Should Contain Link         //*[@id="block_top_menu"]//*[@title="T-shirts"]
    Page Should Contain Link         //*[@id="block_top_menu"]//*[@title="Blouses"]
    Page Should Contain Link         //*[@id="block_top_menu"]//*[@title="Dresses"]
    Page Should Contain Link         //*[@id="block_top_menu"]//*[@title="Casual Dresses"]
    Page Should Contain Link         //*[@id="block_top_menu"]//*[@title="Evening Dresses"]
    Page Should Contain Link         //*[@id="block_top_menu"]//*[@title="Summer Dresses"]
    Page Should Contain Image        //*[@id="category-thumbnail"]//*[@src="http://automationpractice.com/img/c/3-0_thumb.jpg"]
    Page Should Contain Image        //*[@id="category-thumbnail"]//*[@src="http://automationpractice.com/img/c/3-1_thumb.jpg"]
    Capture Page Screenshot

Check products of the category
    Wait Until Element Is Visible    //*[@id="center_column"]
    Title Should Be                  Summer Dresses - My Store
    Page Should Contain Element      //*[@id="center_column"]//*[@class="heading-counter"][contains(text(),"There are 3 products.")]
    Page Should Contain Element      //*[@id="center_column"]//*[@class="rte"][contains(text(),"Short dress, long dress, silk dress, printed dress, you will find the perfect dress for summer.")]
    Page Should Contain Image        //*[@src="http://automationpractice.com/img/p/1/2/12-home_default.jpg"]
    Capture Page Screenshot

Check if the product has been listed on the site
    [Arguments]  ${PRODUCT}  ${PRICE}
    Wait Until Element Is Visible    css=#center_column > h1
    Title Should Be                  Search - My Store
    Page Should Contain Image        //*[@id="center_column"]//*[@src="http://automationpractice.com/img/p/7/7-home_default.jpg"]
    Page Should Contain Link         //*[@id="center_column"]//*[@class="product-name"][contains(text(),"${PRODUCT}")]
    Page Should Contain Element      //*[@id="center_column"]//*[@class="content_price"]//*[@itemprop="price"][contains(text(),"${PRICE}")]
    Page Should Contain Button       //*[@id="center_column"]//*[@class="btn btn-default button button-medium bt_compare bt_compare"]
    Capture Page Screenshot

the product should to be on checkout page
    [Arguments]  ${PRODUCT}  ${PRICE}
     Wait Until Element Is Visible    css=#center_column > h1
     Title Should Be                  Order - My Store
     Page Should Contain Image        //*[@id="center_column"]//*[@src="http://automationpractice.com/img/p/1/1-small_default.jpg"]
     Page Should Contain Link         //*[@id="product_1_1_0_0"]//*[@class="product-name"]/a[contains(text(),"${PRODUCT}")]
     Page Should Contain Element      //*[@id="product_1_1_0_0"]//*[@class="price"][contains(text(),"${PRICE}")]
     Capture Page Screenshot
