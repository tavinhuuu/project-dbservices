*** keywords ***
I search a product name
  [Arguments]  ${PRODUCT}
  Enter the product name in the Search field  ${PRODUCT}
  Click on the search button

I add product to cart
    [Arguments]  ${PRODUCT}
    Enter the product name in the search field  ${PRODUCT}
    Click on the search button
    Click on the product button  Add to cart
    Click on the button  Proceed to checkout

I create account
    Click on the upper right button  Sign in
    Enter a valid email
    Click on the button to create the account  Create an account
    Fill in the required fields
    Click on Register to finish the registration

I search a products name by sub categories
    [Arguments]  ${CATEGORY}   ${SUB_CATEGORY}
    Hover over the category in the top main category menu  ${CATEGORY}
    Check displayed sub categories
    Click on the sub category  ${SUB_CATEGORY}

Enter the product name in the Search field
    [Arguments]  ${PRODUCT}
    Input Text    name=search_query    ${PRODUCT}

Click on the search button
  Click Element    name=submit_search

Hover over the category in the top main category menu
  [Arguments]  ${CATEGORY}
  Wait Until Element Is Visible    //*[@id="block_top_menu"]//*[@class="sf-with-ul"][contains(text(),"${CATEGORY}")]
  Mouse Over                       //*[@id="block_top_menu"]//*[@class="sf-with-ul"][contains(text(),"${CATEGORY}")]

Click on the sub category
  [Arguments]  ${SUB_CATEGORY}
  Click Link                       //*[@id="block_top_menu"]//*[@title="${SUB_CATEGORY}"]

Click on the product button
  [Arguments]  ${ADD_BUY}
  Wait Until Element Is Visible    //*[@id="center_column"]//*[@class="replace-2x img-responsive"]
  Mouse Over                       //*[@id="center_column"]//*[@class="replace-2x img-responsive"]
  Click Element                    //*[@id="center_column"]//*[@title="${ADD_BUY}"]

Click on the button
  [Arguments]  ${CHECKOUT}
  Wait Until Element Is Visible    //*[@id="search"]//*[@title="${CHECKOUT}"]
  Click Element                    //*[@id="search"]//*[@title="${CHECKOUT}"]

Click on the upper right button
  [Arguments]  ${LOGIN}
  Wait Until Element Is Visible    //*[@id="header"]//*[@class="login"][contains(text(),"${LOGIN}")]
  Click Element                    //*[@id="header"]//*[@class="login"][contains(text(),"${LOGIN}")]
  Wait Until Element Is Visible    //*[@id="SubmitCreate"]
  Page Should Contain Element      //*[@id="SubmitCreate"]

Enter a valid email
  Wait Until Element Is Visible    //*[@id="email_create"]
  ${RANDOM} =  Generate Random String  3  [NUMBERS]
  Input Text                       //*[@id="email_create"]   buzzacarini_${RANDOM}@teste.com.br

Click on the button to create the account
  [Arguments]  ${CREATE_ACCOUNT}
  Wait Until Element Is Visible    //*[@id="SubmitCreate"]
  Click Element                    //*[@id="SubmitCreate"]
  Wait Until Element Is Visible    //*[@class="page-heading"][contains(text(),"${CREATE_ACCOUNT}")]   timeout=15
  Page Should Contain Element      //*[@class="page-heading"][contains(text(),"${CREATE_ACCOUNT}")]

Fill in the required fields
  Input Text                       //*[@id="customer_firstname"]  Teste do nome da Maria
  Input Text                       //*[@id="customer_lastname"]   Teste do Sobrenome da Maria
  Input Password                   //*[@id="passwd"]              ABC123abc123
  Input Text                       //*[@id="firstname"]           Teste Endereço primeiro nome
  Input Text                       //*[@id="lastname"]            Teste Endereço secundario nome
  Input Text                       //*[@id="address1"]            Teste do Endereço completo
  Input Text                       //*[@id="city"]                Teste da Cidade
  Select From List By Value        //*[@id="id_state"]            6
  Input Text                       //*[@id="postcode"]            12345
  Select From List By Value        //*[@id="id_country"]          21
  Input Text                       //*[@id="phone_mobile"]        99558877445

Click on Register to finish the registration
  Wait Until Element Is Visible    //*[@id="account-creation_form"]//*[@id="submitAccount"]
  Click Element                    //*[@id="account-creation_form"]//*[@id="submitAccount"]
  Page Should Contain Element      //*[@class="info-account"][contains(text(),"Welcome to your account. Here you can manage all of your personal information and orders.")]
