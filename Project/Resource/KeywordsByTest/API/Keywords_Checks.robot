*** keywords ***
the API Rate should return a valid contract
    [Arguments]  ${DAY}  @{CURRENCY}
    Should Contain  ${resp.text}  {"base":"EUR","rates":{"
    Should Contain  ${resp.text}  "date":"${DAY}"}
    FOR  ${Index}   IN RANGE   1  ${QTT_CURRENCY}
        Should Contain  ${resp.text}  @{CURRENCY}[${Index}]
        # Para o ano de 1999 há menos comparações a fazer, somente 26
        Exit For Loop If  '${DAY}'=='1999-01-04' and '${Index}'=='26'
    END

the API Rate should return a error result
    [Arguments]  ${msg}
    Should Contain  ${resp.text}   ${msg}
