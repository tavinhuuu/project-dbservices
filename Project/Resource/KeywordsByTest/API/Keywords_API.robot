*** keywords ***
I format day for today
    Format current date   %Y-%m-%d
    Log  DATE: ${dt_current}  console=Yes

I format day for yesterday
    Format yesterday date   %Y-%m-%d
    Log  DATE: ${dt_yesterday}  console=Yes

I set variable for specific day
    [Arguments]  ${DATE}
    Set Test Variable    ${specific_day}  ${DATE}
    Log  DATE: ${specific_day}  console=Yes
