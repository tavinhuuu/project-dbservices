*** Settings ***
Library  SeleniumLibrary
Library  String
Library  RequestsLibrary
Library  DateTime


Resource     ../Input/Variables.robot

Resource     Utils/Formatting_dates.robot

Resource     KeywordsByTest/API/Keywords_API.robot
Resource     KeywordsByTest/API/Keywords_Checks.robot

Resource     KeywordsByTest/UI/Keywords_UI.robot
Resource     KeywordsByTest/UI/Keywords_Commons.robot
Resource     KeywordsByTest/UI/Keywords_Checks.robot
